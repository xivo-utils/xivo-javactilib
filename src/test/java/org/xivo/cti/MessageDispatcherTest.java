package org.xivo.cti;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.xivo.cti.listener.ConfigListener;
import org.xivo.cti.message.UserConfigUpdate;


public class MessageDispatcherTest {
    @Rule
    public JUnitRuleMockery context = new JUnitRuleMockery();
    private MessageDispatcher messageDispatcher;
    final ConfigListener userConfigUpdateListener = context.mock(ConfigListener.class);

    @Before
    public void setUp() throws Exception {
        messageDispatcher = new MessageDispatcher();
    }

    @Test
    public void oneSubscriberReceivesCtiEvent() {
        final UserConfigUpdate userConfigUpdate = new UserConfigUpdate();
        
        messageDispatcher.addListener(UserConfigUpdate.class, userConfigUpdateListener);
        
        context.checking(new Expectations() {{
            oneOf (userConfigUpdateListener).onUserConfigUpdate(userConfigUpdate);
        }});

        messageDispatcher.dispatch(userConfigUpdate);
    }

    @Test
    public void sameSubscriberReceivesOnlyOnceCtiEvent() {
        final UserConfigUpdate userConfigUpdate = new UserConfigUpdate();
        
        messageDispatcher.addListener(UserConfigUpdate.class, userConfigUpdateListener);
        messageDispatcher.addListener(UserConfigUpdate.class, userConfigUpdateListener);
        
        context.checking(new Expectations() {{
            oneOf (userConfigUpdateListener).onUserConfigUpdate(userConfigUpdate);
        }});

        messageDispatcher.dispatch(userConfigUpdate);
    }

}
