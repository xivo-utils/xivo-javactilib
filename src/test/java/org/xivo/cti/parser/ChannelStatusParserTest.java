package org.xivo.cti.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.ChannelStatus;

public class ChannelStatusParserTest {
    private ChannelStatusParser channelStatusParser;

    @Before
    public void setUp() throws Exception {
        channelStatusParser = new ChannelStatusParser();
    }

    @Test
    public void parseChannelStatusUpdate() throws JSONException {
        JSONObject jsonChannelStatusUpdate = new JSONObject("{\"function\": \"updatestatus\", " +
                "\"listname\": \"channels\", " +
                "\"tipbxid\": \"xivo\", " +
                "\"timenow\": 1378136055.76, " +
                "\"status\": {\"timestamp\": 1378136055.65, \"holded\": true, \"commstatus\": \"ready\",\"parked\": true, \"state\": \"Down\"}, " +
                "\"tid\": \"SIP/ihvbur-00000001\", \"class\": \"getlist\"}");

        ChannelStatus channelStatus = channelStatusParser.parseStatusUpdate(jsonChannelStatusUpdate);

        assertNotNull("unable de decode channel status update", channelStatus);
        assertEquals("unable to decode channel id ","SIP/ihvbur-00000001",channelStatus.channelId);
        assertEquals("unable to decode timestamp",1378136055.65,channelStatus.timeStamp,0.001);
        assertTrue("unable to decode holded",channelStatus.holded);
        assertEquals("unable to decode commstatus","ready",channelStatus.commstatus);
        assertTrue("unable to decode parked",channelStatus.parked);
        assertEquals("unable to decode state","Down",channelStatus.state);

    }

    @Test
    public void parseChannelStatusUpdateOptionalFields() throws JSONException {
        JSONObject jsonChannelStatusUpdate = new JSONObject("{\"function\": \"updatestatus\", " +
                "\"listname\": \"channels\", " +
                "\"tipbxid\": \"xivo\", " +
                "\"timenow\": 1378136055.76, " +
                "\"status\": {\"talkingto_kind\": \"channel\", \"direction\" : \"in\", \"talkingto_id\": \"SIP/x2gjtw-0000000b\"," +
                "\"timestamp\": 1378136055.65, \"holded\": true, \"commstatus\": \"ready\",\"parked\": true, \"state\": \"Down\"}, " +
                "\"tid\": \"SIP/ihvbur-00000001\", \"class\": \"getlist\"}");

        ChannelStatus channelStatus = channelStatusParser.parseStatusUpdate(jsonChannelStatusUpdate);

        assertNotNull("unable de decode channel status update", channelStatus);
        assertEquals("unable to decode direction","in",channelStatus.direction);
        assertEquals("unable to decode talkingto_kind","channel",channelStatus.talkingto_kind);
        assertEquals("unable to decode talkingto_id","SIP/x2gjtw-0000000b",channelStatus.talkingto_id);

    }

}
