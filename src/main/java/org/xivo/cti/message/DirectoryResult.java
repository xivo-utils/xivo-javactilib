package org.xivo.cti.message;

import java.util.ArrayList;
import java.util.List;

import org.xivo.cti.listener.DirectoryListener;
import org.xivo.cti.model.DirectoryEntry;

public class DirectoryResult extends CtiResponseMessage<DirectoryListener> {

    private final List<String> headers = new ArrayList<String>();
    private final List<DirectoryEntry> entries = new ArrayList<DirectoryEntry>();

    @Override
    public void notify(DirectoryListener listener) {
        listener.onDirectoryResult(this);
    }

    public List<String> getHeaders() {
        return headers;
    }

    public void addHeader(String header) {
        headers.add(header);
    }

    public List<DirectoryEntry> getEntries() {
        return entries;
    }

    public void addEntry(DirectoryEntry entry) {
        entries.add(entry);
    }

}