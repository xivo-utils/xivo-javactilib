package org.xivo.cti.message;

import org.xivo.cti.CtiMessage;

public abstract class CtiResponseMessage  <L>  extends CtiMessage{
    abstract public void notify(final L listener);
}
