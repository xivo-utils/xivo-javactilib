package org.xivo.cti.message.request;

public class Dnd extends CtiRequest {
    private String function = "enablednd";
    private boolean state = false;
    
    public Dnd(boolean state) {
        super("featuresput");
        this.state = state;
    }
    public String getFunction() {
        return function;
    }
    public boolean getState() {
        return state;
    }
}