package org.xivo.cti.message.request;

/**
 * Created by jylebleu on 26/08/14.
 */
public class AgentQueueConfig extends  IpbxCommand {
    protected static final String QUEUEADD = "queueadd";
    protected static final String QUEUEREMOVE = "queueremove";

    private String member = "";
    private String queue = "";

    public AgentQueueConfig(String command , String agentId, String queueId) {
        super(command);
        this.member = "agent:" + XIVO + "/" + agentId;
        this.queue = "queue:" + XIVO + "/" + queueId;

    }
    public String getMember() {
        return member;
    }

    public String getQueue() {
        return queue;
    }
}
