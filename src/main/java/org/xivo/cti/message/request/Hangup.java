package org.xivo.cti.message.request;

public class Hangup extends CtiRequest {
    public Hangup() {
        super("hangup");
    }

}