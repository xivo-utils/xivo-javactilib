package org.xivo.cti.message.request;

import org.xivo.cti.model.ObjectType;


public class GetAgents extends GetConfig {
    private static final String LISTID = "listid";

    public GetAgents() {
        super(LISTID,ObjectType.AGENTS.toString());
    }

}
