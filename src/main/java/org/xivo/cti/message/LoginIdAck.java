package org.xivo.cti.message;

import org.xivo.cti.listener.LoginStepListener;

public class LoginIdAck extends CtiResponseMessage<LoginStepListener>{

	public String sesssionId;
	public double timenow;
	public String xivoversion;

    @Override
    public void notify(LoginStepListener listener) {
        listener.onLoginIdAck(this);
    }

}
