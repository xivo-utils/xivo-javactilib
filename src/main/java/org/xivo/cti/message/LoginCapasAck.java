package org.xivo.cti.message;

import java.util.List;

import org.xivo.cti.listener.LoginStepListener;
import org.xivo.cti.model.Capacities;
import org.xivo.cti.model.Xlet;

public class LoginCapasAck extends CtiResponseMessage<LoginStepListener> {

	public String presence;
	public String userId;
	public String applicationName;
	public Capacities capacities;
	public List<Xlet> xlets;

    @Override
    public void notify(LoginStepListener listener) {
        listener.onLoginCapasAck(this);

    }

}
