package org.xivo.cti.message;

import org.xivo.cti.listener.AgentUpdateListener;
import org.xivo.cti.model.AgentStatus;

public class AgentStatusUpdate extends CtiResponseMessage<AgentUpdateListener> {

    private final int agentId;
    private final AgentStatus status;

    public AgentStatusUpdate(int agentId, AgentStatus status) {
        this.agentId = agentId;
        this.status = status;
    }

    public int getAgentId() {
        return this.agentId;
    }

    public AgentStatus getStatus() {
        return status;
    }

    @Override
    public void notify(AgentUpdateListener listener) {
        listener.onAgentStatusUpdate(this);
    }

}
