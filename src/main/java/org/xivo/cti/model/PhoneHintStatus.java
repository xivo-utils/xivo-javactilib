package org.xivo.cti.model;

public enum PhoneHintStatus {
	ONHOLD (16),
	RINGING (8),
	INDISPONIBLE (4),
	BUSY_AND_RINGING (9),
	AVAILABLE (0),
	CALLING (1),
	BUSY (2),
	DEACTIVATED (-1),
	UNEXISTING (-2),
	ERROR (-99);
	
	private final int hintstatus;
	PhoneHintStatus(int hintstatus) {
		this.hintstatus = hintstatus;
	}
	
	public int getHintStatus() {
		return this.hintstatus;
	}
	
	public static PhoneHintStatus getHintStatus(int id) {
		switch(id) {
			case 16:
				return PhoneHintStatus.ONHOLD;
			
			case 8:
				return PhoneHintStatus.RINGING;
			
			case 4:
				return PhoneHintStatus.INDISPONIBLE;
			
			case 9:
				return PhoneHintStatus.BUSY_AND_RINGING;
				
			case 0: 
				return PhoneHintStatus.AVAILABLE;
				
			case 1:
				return PhoneHintStatus.CALLING;
				
			case 2:
				return PhoneHintStatus.BUSY;
				
			case -1:
				return PhoneHintStatus.DEACTIVATED;
				
			case -2:
				return PhoneHintStatus.UNEXISTING;
				
			default:
				return PhoneHintStatus.ERROR;				
		}
	}
}
