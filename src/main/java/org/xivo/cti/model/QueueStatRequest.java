package org.xivo.cti.model;

public class QueueStatRequest {
    private String queueId;
    private int window;
    private int xqos;

    public QueueStatRequest(String queueId, int window, int xqos) {
        this.queueId = queueId;
        this.window = window;
        this.xqos = xqos;
    }

    public String getQueueId() {
        return queueId;
    }

    public int getWindow() {
        return window;
    }

    public int getXqos() {
        return xqos;
    }

}
