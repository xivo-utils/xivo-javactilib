package org.xivo.cti.model;

import java.util.ArrayList;
import java.util.List;

public class DirectoryEntry {
    private final List<String> fields = new ArrayList<String>();

    public void addField(String field) {
        fields.add(field);
    }

    public List<String> getFields() {
        return fields;
    }


    @Override
    public boolean equals(Object obj) {
        DirectoryEntry entry = (DirectoryEntry) obj;
        for (String field : entry.fields) {
            if (!fields.contains(field))
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String res = "";
        for (String field : fields) {
            res = res + " - " + field;
        }
        return res;
    }

}
