package org.xivo.cti.parser;

import java.util.Date;

public class DateParser {

    public static Date parse(Double unixTimestamp) {
        return new Date(new Double(unixTimestamp * 1000).longValue());
    }
}
